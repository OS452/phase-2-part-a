#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>

int P4_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 0;
        int* pid = &x; 

        int pid2 = Sys_Spawn("P4_Startup", P4_Startup, NULL, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(pid2 == 0);
        assert(*pid > 3 && *pid < P1_MAXPROC);
        
       
        *pid = 0;
        Sys_CPUTime(pid);
        assert(*pid > 0);
     
       
        x = 0;
        pid = &x;
        Sys_GetTimeofDay(pid);
        assert(*pid > 0); 


        x = 0;
        pid = &x;
        Sys_GetPID(pid);
        assert(*pid == 3);

        int *mbox = (int*) malloc(sizeof(int));;
        Sys_MboxCreate(10, 10, mbox);
        assert(*mbox == 1);

        int KK = 8;
        int* size = &KK;
        char* buffer = malloc(8);
        int result = Sys_MboxCondReceive(*mbox,buffer, size); 
        assert(result == 1);


        int OK = 8;
        size = &OK;
        Sys_MboxSend(*mbox, "Working", size);

    
        KK = 8;
        size = &KK;
        buffer = malloc(8);
        Sys_MboxReceive(*mbox,buffer, size);
        assert(*size == 8); 
        
        KK = 8;
        size = &KK;
        buffer = malloc(8);
        int received = Sys_MboxCondReceive(*mbox,buffer, size); 
        assert(received == 1);

	return 7;
}

int P4_Startup(void* arg) {
    //USLOSS_Console("HERE\n");
    return 0;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
