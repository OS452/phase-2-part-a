#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


int P4_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 0;
        int* pid = &x;         
        

        int *mbox = (int*) malloc(sizeof(int));
        Sys_MboxCreate(1, 10, mbox);
        assert(*mbox == 1);


        int pid2 = Sys_Spawn("P4_Startup", P4_Startup, mbox, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(pid2 == 0);
        assert(*pid > 3 && *pid < P1_MAXPROC);

        USLOSS_Console("Sending message 1\n");
        int Ok = 8;
        int* size = &Ok;
        Sys_MboxSend(*mbox, "Working", size);
        USLOSS_Console("Done sending 1\n");


        printf("Sending message 2, should block since mailbox is full until P4 receives\n");
        Ok = 8;
        size = &Ok;
        Sys_MboxSend(*mbox, "Working", size);
        printf("Done sending 2\n");
        return 7;
}

int P4_Startup(void* arg) {
    int* mbox = (int*) arg;

    int KK = 8;
    int* size = &KK;
    char* buffer = malloc(8);
    USLOSS_Console("Going to try to receive message 1\n");
    Sys_MboxReceive(*mbox,buffer, size);
    USLOSS_Console("Done receiving message 1\n");
    assert(*size == 8);

    return 0;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
