/*
 * simpleClockDriverTest.c
 *
 *  Created on: Mar 9, 2015
 *      Author: tishihara
 */

#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


/*
 * This tests error conditions for Sys_Spawn, it should compile and if not it fails
 * Should print: NOT GOOD! \n KERNEL MODE REQUIRED!
 * */

int P4_Startup(void *arg);
int P5_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 0;
        int* pid = &x;
        int result;
        int i;

        result = Sys_Spawn("P4_Startup", P4_Startup, NULL, 0, 4, pid);
        assert(result == -1);
        assert(*pid == -1);
 
        result = Sys_Spawn("P4_Startup", P4_Startup, NULL, 4 *  USLOSS_MIN_STACK, 12, pid);
        assert(result == -1);
        assert(*pid == -1);
 
        char* argument = "HELLO";
        for (i = 0; i < P1_MAXPROC-5; i++) {
             result = Sys_Spawn("P4_Startup", P4_Startup, argument, 4 *  USLOSS_MIN_STACK, 4, pid);
             assert(result == 0);
             assert(0 <= *pid && *pid < P1_MAXPROC);
        }

         int y = 6;
        
        result = Sys_Spawn("P5_Startup", P5_Startup, (void*) y, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(result == 0);
        assert(0 <= *pid && *pid < P1_MAXPROC);

        //Sys_DumpProcesses();


        result = Sys_Spawn("P4_Startup", P4_Startup, NULL, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(*pid == -1);
        assert(result == 0);
       
        return 7;
}

int P4_Startup(void* arg) {
    char* string = (char*) arg;
    assert(strcmp(string, "HELLO") == 0);
    return 0;
}

int P5_Startup(void* arg) {
    
    int argument = (int) arg;
    assert(argument == 6);

    int pid = P1_GetPID();
    assert(pid < 0);
    return 7;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
