/*
 * simpleClockDriverTest.c
 *
 *  Created on: Mar 9, 2015
 *      Author: tishihara
 */

#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


/*
 * This tests error and correct conditions for Sys_Wait 
 * */

int P4_Startup(void *arg);
int P5_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 12;
        int* pid = &x;
        int* status = &x;

        int result = Sys_Wait(pid, status);
        assert(result == -1);

        char* argument = "HELLO";
        int i;
        for (i = 0; i < P1_MAXPROC-5; i++) {
             result = Sys_Spawn("P4_Startup", P4_Startup, argument, 4 *  USLOSS_MIN_STACK, 4, pid);
             assert(result == 0);
             assert(0 <= *pid && *pid < P1_MAXPROC);
        }     

     
       for (i = 0; i < P1_MAXPROC-5; i++) {
             result = Sys_Wait(pid, status);
             assert(result == 0);
             assert(*status == 13);
             assert(*pid >= 0 && *pid < P1_MAXPROC);
       }     

        result = Sys_Wait(pid, status);
        assert(result == -1);


       return 7;
}

int P4_Startup(void* arg) {
    char* string = (char*) arg;
    assert(strcmp(string, "HELLO") == 0);
    return 13;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
