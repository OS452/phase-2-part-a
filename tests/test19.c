#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


/*
 * This tests error conditions for P2 Kernel Protection 
 * */


int P3_Startup(void *arg) {
        
        //This prints a kernel function error  
        P2_Terminate(9);
       
        //This prints a kernel function error
        P2_Sleep(12);

        //This prints a kernel function error
        P2_MboxCreate(10, 1);
       
        int Ok = 7;
        int* status = &Ok;
        
        // This prints a kernel function error
        P2_Wait(status); 
       
        // This prints a kernel function error  
        P2_MboxRelease(0);
    
        // This prints a kernel function error
        P2_MboxSend(0, "HELLO", status);
       
        // This prints a kernel function error
        P2_MboxCondSend(0, "HELLO", status);
 
        void * buff = malloc(4);
        // This prints a kernel function error
        P2_MboxReceive(0, buff, status);

        // This prints a kernel function error
        P2_MboxCondReceive(0, buff, status);

        // This prints a kernel function error
        P2_Spawn("P3_Startup", P3_Startup, NULL, 4 *  USLOSS_MIN_STACK, 4);

        
       return 7;
}


void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
