#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


int P4_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 0;
        int* pid = &x;         
        

        int *mbox = (int*) malloc(sizeof(int));
        Sys_MboxCreate(10, 10, mbox);
        assert(*mbox == 1);


        int pid2 = Sys_Spawn("P4_Startup", P4_Startup, mbox, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(pid2 == 0);
        assert(*pid > 3 && *pid < P1_MAXPROC);

        int KK = 12;
        int* size = &KK;
        char* buffer = malloc(8);
        
        USLOSS_Console("Going to try to receive message, should block until P4 sends a message\n");
        Sys_MboxReceive(*mbox,buffer, size);
        USLOSS_Console("Received message!\n");
        assert(*size == 8);

     
        
	return 7;
}

int P4_Startup(void* arg) {
    int* mbox = (int*) arg;

    USLOSS_Console("Sending message\n");
    int Ok = 8;
    int* size = &Ok;
    Sys_MboxSend(*mbox, "Working", size);
    USLOSS_Console("Done sending\n");
    return 0;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
